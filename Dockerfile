# syntax=docker/dockerfile:1

FROM node:12.18.1
ENV NODE_ENV=production

WORKDIR /app

COPY ["package.json", "package-lock.json*", "./"]
RUN npm i typescript ts-node -g
RUN npm install --production

COPY . .
RUN npm run build

CMD [ "node", "dist/index.js" ]
