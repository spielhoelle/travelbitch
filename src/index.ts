import * as dotenv from "dotenv";
let cachedResp: {}[] = []
import * as path from 'path'
const thePath = path.join(__dirname, '/../', '.env')
dotenv.config({ path: thePath });
import axios from 'axios';

const run = async (): Promise<void> => {
	const url = `https://www.japan-guide.com/news/alerts.html`
	try {
		const resp2 = await axios.request({
			baseURL: url,
			headers: {
				"Content-type": "application/json"
			}
		});

		cachedResp = resp2.data
		const borderOpened = cachedResp.indexOf('borders closed to individual travelers') === -1
		console.log(new Date(), "BorderOpened: ", borderOpened)
		if(borderOpened){
			await axios.post(`https://api.telegram.org/bot${process.env.TELEGRAM_BOT_ID}/sendMessage`, {
				chat_id: process.env.CHAT_ID,
				text: `Something changed on https://www.japan-guide.com/news/alerts.html`
			})
		}
	} catch (error) {
		console.log('error', error);
	}

}
(async () => {
	try {
		run()
	} catch (error) {
		console.error(error)
	}
})()
